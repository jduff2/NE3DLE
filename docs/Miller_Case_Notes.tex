% ***********************************************************
% ******************* PHYSICS HEADER ************************
% ***********************************************************
% Version 2
\documentclass[12pt]{article}
\usepackage{amsmath} % AMS Math Package
\usepackage{amsthm} % Theorem Formatting
\usepackage{amssymb}    % Math symbols such as \mathbb
\usepackage{isomath}
\usepackage{graphicx} % Allows for eps images
\usepackage[dvips,letterpaper,margin=0.7in,bottom=0.7in]{geometry}
\usepackage{tensor}
\usepackage{cancel}
\usepackage{color}
\usepackage{float}
 % Sets margins and page size

\renewcommand{\labelenumi}{(\alph{enumi})} % Use letters for enumerate
% \DeclareMathOperator{\Sample}{Sample}
\let\vaccent=\v % rename builtin command \v{} to \vaccent{}
\usepackage{enumerate}
\renewcommand{\v}[1]{\ensuremath{\mathbf{#1}}} % for vectors
\newcommand{\gv}[1]{\ensuremath{\mbox{\boldmath$ #1 $}}} 
% for vectors of Greek letters	
\newcommand{\uv}[1]{\ensuremath{\mathbf{\hat{#1}}}} % for unit vector
\newcommand{\abs}[1]{\left| #1 \right|} % for absolute value
\newcommand{\avg}[1]{\left< #1 \right>} % for average
\let\underdot=\d % rename builtin command \d{} to \underdot{}
\renewcommand{\d}[2]{\frac{d #1}{d #2}} % for derivatives
\newcommand{\dd}[2]{\frac{d^2 #1}{d #2^2}} % for double derivatives
\newcommand{\pd}[2]{\frac{\partial #1}{\partial #2}} 
% for partial derivatives
\newcommand{\pdd}[2]{\frac{\partial^2 #1}{\partial #2^2}} 
% for double partial derivatives
\newcommand{\pdm}[3]{\frac{\partial^2 #1}{\partial #2 \partial #3}}
\newcommand{\pdc}[3]{\left( \frac{\partial #1}{\partial #2}
 \right)_{#3}} % for thermodynamic partial derivatives
\newcommand{\ket}[1]{\left| #1 \right>} % for Dirac bras
\newcommand{\bra}[1]{\left< #1 \right|} % for Dirac kets
\newcommand{\braket}[2]{\left< #1 \vphantom{#2} \right|
 \left. #2 \vphantom{#1} \right>} % for Dirac brackets
\newcommand{\matrixel}[3]{\left< #1 \vphantom{#2#3} \right|
 #2 \left| #3 \vphantom{#1#2} \right>} % for Dirac matrix elements
\newcommand{\grad}[1]{\gv{\nabla} #1} % for gradient
\let\divsymb=\div % rename builtin command \div to \divsymb
\renewcommand{\div}[1]{\gv{\nabla} \cdot \v{#1}} % for divergence
\newcommand{\curl}[1]{\gv{\nabla} \times \v{#1}} % for curl
\let\baraccent=\= % rename builtin command \= to \baraccent
\renewcommand{\=}[1]{\stackrel{#1}{=}} % for putting numbers above =
\providecommand{\wave}[1]{\v{\tilde{#1}}}
\providecommand{\fr}{\frac}
\providecommand{\RR}{\mathbb{R}}
\providecommand{\NN}{\mathbb{N}}
\providecommand{\seq}{\subseteq}
\providecommand{\e}{\epsilon}
\newcommand{\vs}[1]{\vectorsym{#1}}
\newcommand{\ts}[1]{\tensorsym{#1}}

\newtheorem{prop}{Proposition}
\newtheorem{thm}{Theorem}[section]
\newtheorem{axiom}{Axiom}[section]
\newtheorem{p}{Problem}[section]
\usepackage{cancel}
\newtheorem*{lem}{Lemma}
\theoremstyle{definition}
\newtheorem*{dfn}{Definition}
 \newenvironment{s}{%\small%
        \begin{trivlist} \item \textbf{Solution}. }{%
            \hspace*{\fill} $\blacksquare$\end{trivlist}}%
            
\newcommand{\kyps}{k_y\rho_{\text{s}}}
\newcommand{\ion}{\text{i}}
\newcommand{\elec}{\text{e}}
\newcommand{\phit}{\widetilde{\Phi}}
\newcommand{\wci}{\Omega_{\text{ci}}}
\newcommand{\Te}{T_\elec}
\newcommand{\Ti}{T_\ion}
\newcommand{\Tmw}{\frac{\Te}{m_\ion \wci}}
\newcommand{\abpsi}{\abs{\grad\psi}}
\newcommand{\psisq}{\abpsi^2}
\newcommand{\curvn}{\kappa_n}
\newcommand{\curvg}{\kappa_g}
\newcommand{\bhat}{\vs{\hat{b}}}
\newcommand{\nhat}{\vs{\hat{n}}}
\newcommand{\Rz}{R_{,\zeta}}
\newcommand{\Rt}{R_{,\theta}}
\newcommand{\Rzz}{R_{,\zeta\zeta}}
\newcommand{\Rzt}{R_{,\zeta\theta}}
\newcommand{\Rtz}{R_{,\theta\zeta}}
\newcommand{\Rtt}{R_{,\theta\theta}}
\newcommand{\Zz}{Z_{,\zeta}}
\newcommand{\Zt}{Z_{,\theta}}
\newcommand{\Zzz}{Z_{,\zeta\zeta}}
\newcommand{\Zzt}{Z_{,\zeta\theta}}
\newcommand{\Ztz}{Z_{,\theta\zeta}}
\newcommand{\Ztt}{Z_{,\theta\theta}}
\newcommand{\ibar}{{\mbox{$\,\iota\!\!$-}}}
\newcommand{\Bref}{B_{\textrm{ref}}}
\newcommand{\Lref}{L_{\textrm{ref}}}
%\newcommand{\cos}{\,\textrm{cos}}
%\newcommand{\sin}{\,\textrm{sin}}
% ***********************************************************
% ********************** END HEADER *************************
% ***********************************************************

\begin{document}
\section*{Numerical implementation of Miller Equilibrium for local 3D equilibrium}
The Miller equilibrium is a set of nine dimensionless parameters for a noncircular, finite aspect ratio local equilibrium. The Miller equilibrium prescribes the flux surface shape, poloidal field on the surface, global shear, and pressure gradient.
The surface shaping is prescribed by the following equations:
\begin{equation}
\label{eqn:R}
R = R_0 + r\cos(\theta + (\arcsin\delta)\sin\theta)
\end{equation}
\begin{equation}
\label{eqn:Z}
Z = \kappa r\sin(\theta)
\end{equation}
where $R_0$ and $r$ are the major and minor radius of the flux surface (controlled by the aspect ratio $A = R_0/r$), respectively, $\theta$ is the geometric poloidal angle, $\delta$ is the triangularity, $\kappa$ is the elongation. The poloidal field is described with four additional variables: three radial derivatives of shaping parameters $s_{\kappa}$, $s_{\delta}$, and $d_rR_0$, and $q$.
\begin{equation}
B_p = \frac{d_r\psi_p[\sin^2(\theta + x\sin\theta)(1+x\cos\theta)^2+\kappa^2\cos^2\theta]^{1/2}}{\kappa R a_0},
\end{equation}
where $a_0 = \cos(x\sin\theta) + d_rR_0\cos\theta + [s_{\kappa}-s_{\delta}\cos\theta + (1+s_{\kappa})x\cos\theta]\sin\theta\sin(\theta+x\sin\theta)$, $\sin x = \delta$, $d_r\psi_p$ is the radial derivative of the poloidal flux consistent with $q=(f/2\pi)\int dl_p/(R^2B_p)$, $f(\psi_p)=RB_{\phi}$,  $dl_p$ is the differential poloidal arc length, and $B_{\phi}$ is the toroidal magnetic field. The notation $d_rg$ means $dg/dr$. The flux surface averaged magnetic shear and pressure gradient are parameterized with $\hat{s} = d\ln{q}/d\ln{r}$ and $\alpha$, respectively:
\begin{equation}
\hat{s} = -\frac{B_0 \rho^2}{\ibar}\d{\ibar}{\psi}
\end{equation}
\begin{equation}
\alpha = -\frac{2\mu_0 \rho R_0}{\ibar^2B_0}\d{p}{\psi},
\end{equation}
where $\psi$ is the toroidal flux and $\ibar$ is the rotational transform $\ibar=1/q$, and $\rho$ is a radial-like coordinate such that the toroidal flux $\psi = \rho^2 \Bref/2$. \\ \\
\noindent To summarize, the Miller equilibrium has nine dimensionless parameters, $A$, $\kappa$, $\delta$, $s_{\kappa}$, $s_{\delta}$, $d_rR_0$, $q$, $\hat{s}$, and $\alpha$.
\subsection*{Transformation between $\Theta$ and $\theta$}
The Miller equilibrium can be transformed into straight field line angle coordinates $(\Theta,\zeta)$ by using the following differential equation:
\begin{equation}
\label{eqn:TtOde}
\frac{\partial\Theta}{\partial\theta} = \left| \frac{\partial\textbf{X}}{\partial\theta} \right|\frac{f}{qR^2B_p}
\end{equation}
where $\textbf{X} = \textbf{X}(\theta) = \textbf{X}(R(\theta),Z(\theta))$. Taking a closer look at $\left| \frac{\partial\textbf{X}}{\partial\theta} \right|$, we can develop the following relation:
\begin{align}
\left| \frac{\partial\textbf{X}}{\partial\theta} \right| =& \sqrt{\frac{\partial\textbf{X}}{\partial\theta}\cdot\frac{\partial\textbf{X}}{\partial\theta}}\nonumber \\=& \sqrt{\frac{\partial R}{\partial\theta}^2+\frac{\partial Z}{\partial\theta}^2}\nonumber \\ =& \sqrt{ r^2\sin^2(\theta+x\sin\theta)(1+x\cos\theta)^2+ r^2\kappa^2\cos^2\theta}\nonumber \\ =&  r\sqrt{\sin^2(\theta+x\sin\theta)(1+x\cos\theta)^2+\kappa^2\cos^2\theta}\nonumber.
\end{align}
In this form, we notice,
\begin{equation}
    \label{eqn:BpdXdt}
    B_p = \abs{\pd{\v{X}}{\theta}}\fr{d_r\psi_p}{r\kappa R a_0}
\end{equation}
The relationship for $q$ can be expanded by
\begin{align}
q =& \frac{f}{2\pi}\int dl_p/(R^2B_p)\nonumber \\
   =& \frac{f}{2\pi}\int_0^{2\pi} \left| \frac{\partial\textbf{X}}{\partial\theta} \right| \frac{d\theta}{R^2B_p}\nonumber \\ 
   =& \frac{f}{2\pi}\int_0^{2\pi} \frac{ r\kappa a_0 }{Rd_r\psi_p}d\theta\nonumber.
\end{align}
Pulling flux surface quantities $\kappa$, $r$, and $d_r\psi_p$ out of the integrand, we can define
\begin{align}
\gamma = \int_0^{2\pi} \frac{a_0}{R}d\theta
\end{align}
and $q$ becomes
\begin{align}
\label{eqn:q_simp}
q = \gamma\frac{fr\kappa}{2\pi d_r\psi_p}.
\end{align}
The relations in  \eqref{eqn:BpdXdt} and  \eqref{eqn:q_simp} can be substituted into  \eqref{eqn:TtOde} to simplify
\begin{align}
\label{eqn:TtSimp}
\frac{\partial\Theta}{\partial\theta} =& \frac{2\pi}{\gamma}\frac{a_0}{R}.
\end{align}
The constant $\gamma$ can be numerically computed easily, and the mapping and coordinate parameterization required to solve local 3D equilibrium will be computed in the next section.
\subsubsection*{Normalizing radial-like coordinate for GENE input.}
\noindent NE3DLE outputs are used as GENE inputs with \texttt{magn$\_$geom} $=$ \texttt{"gist"}, so NE3DLE normalizations need to be consistent with GIST normalizations. In ``loc3D$\text{\_}$notes", the toroidal flux is assumed to have the form $\psi = \rho^2 \Bref/2$. Then,
\begin{align*}
    \psi = \fr{\rho^2\Bref}{2} = \int \d{\psi}{r}dr = \fr{\gamma R_0 B_0 \kappa r^2}{4\pi},
\end{align*}
using \eqref{eqn:q_simp}, and $f=R_0 B_0$.
Now, we define $\rho$ assuming $\Bref=B_0$:
\begin{equation}
    \rho = \left(\fr{\gamma R_0\kappa}{2\pi}\right)^{1/2}r
\end{equation}
\subsection*{Miller equilibrium coordinate parameterization and mapping}
\subsubsection*{Coordinate parameterization}
To solve the local 3D equilibrium, the variables $theta\_ p$, $theta\_ p\_ theta$, and $theta\_ p\_\_ theta \_ theta$ must be found. The variables translate to $\theta$, $\frac{\partial\theta}{\partial\Theta}$, and $\frac{\partial^2\theta}{\partial\Theta^2}$, respectively. The first two variables are obvious, but, to be explicit,
\begin{align*}
theta =& \theta \\
theta\_ p\_ theta =& \frac{\gamma}{2\pi}\frac{R}{a_0}.
\end{align*}
As for $theta\_ p \_ theta \_ theta$, and expansion can be done
\begin{align*}
theta\_ p \_ theta \_ theta =& \frac{\partial^2\theta}{\partial\Theta^2} \\
 =& \frac{\partial}{\partial\Theta}\frac{\partial\theta}{\partial\Theta} \\
 =& \frac{\gamma}{2\pi}\frac{\partial}{\partial\Theta}\frac{R}{a_0} \\
 =& \frac{\gamma}{2\pi}\frac{\partial\theta}{\partial\Theta}\frac{\partial}{\partial\theta}\frac{R}{a_0}\\
 =& \frac{\gamma}{2\pi}\frac{\partial\theta}{\partial\Theta}\bigg(\frac{1}{a_0}\frac{\partial R}{\partial\theta}-\frac{R}{a_0^2}(-x\cos\theta\sin(x\sin\theta)-d_rR_0\sin\theta+s_{\kappa}(\cos\theta\sin(\theta+x\sin\theta)\\&+\sin\theta(1+x\cos\theta)\cos(\theta+x\sin\theta))+\\&((1+s_{\kappa})x-s_{\delta})(\sin(\theta+x\sin\theta)(\cos^2\theta-\sin^2\theta)\\&+\frac{1}{2}\sin(2\theta)(1+x\cos\theta)\cos(\theta+x\sin\theta)))\bigg).
\end{align*}
Since the system is axisymmetric, $zeta\_p = \zeta$, and $\pd{}{\zeta}=0$. This fully describes the coordinate parameterization.
\subsubsection*{Coordinate mapping}
The local equilibrium code works in $(\Theta,\zeta)$ coordinates, presenting two problems: finding the $\Theta$ and $\zeta$ derivatives of $R(\theta)$ and $Z(\theta)$ , and finding these values on an evenly spaced $(\Theta,\zeta)$ mesh. The $\Theta$ and $\zeta$ derivatives of $R(\theta)$ and $Z(\theta)$ can be found by finding the $\theta$ and $\zeta$ derivatives of $R(\theta)$ and $Z(\theta)$ and using  \eqref{eqn:TtSimp} to transform $\pd{}{\theta}$ to $\pd{}{\Theta}$. Using \eqref{eqn:R} and  \eqref{eqn:Z}, derivatives with respect to $\theta$ can be found:
\begin{align*}
\pd{R}{\theta} =& -r(1+x\cos\theta)\sin(\theta+x\sin\theta)\\
\pd{Z}{\theta} =& \kappa r\cos\theta\\
\pdd{R}{\theta} =& r(x\sin\theta\sin(\theta+x\sin\theta)-(1+x\cos\theta)^2\cos(\theta+x\sin\theta))\\
\pdd{Z}{\theta} =& -\kappa r\sin\theta
\end{align*}
and $\pd{}{\zeta}=0$.
The $\Theta$ derivatives used in computation are as follows:
\begin{align*}
\pd{R}{\Theta} =& \pd{R}{\theta}\pd{\theta}{\Theta}\\
\pd{Z}{\Theta} =& \pd{Z}{\theta}\pd{\theta}{\Theta}\\
\pdd{R}{\Theta} =& \pdd{R}{\theta}\left(\pd{\theta}{\Theta}\right)^2 + \pd{R}{\theta}\pdd{\theta}{\Theta}\\
\pdd{Z}{\Theta} =& \pdd{Z}{\theta}\left(\pd{\theta}{\Theta}\right)^2 + \pd{Z}{\theta}\pdd{\theta}{\Theta}
\end{align*}
and $\pd{}{\zeta}=0$. This fully describes the coordinate mapping, but only on an evenly spaced $(\theta,\zeta)$ grid. This results in and unevenly spaced $\eta$ vector
when computing field line quantities. We can see in \eqref{eqn:TtSimp} $\Theta$ and $\theta$ do not have an analytic relationship, so to move these values onto and evenly spaced 
$(\Theta,\zeta)$ grid, a Fourier decomposition (done in $\theta$ space, because the grid is even spaced) and simple interpolation can be done.
\begin{align*}
    \int d\Theta &=\fr{2\pi}{\gamma}\int\fr{a_0}{R}d\theta\\
    \Theta + const. &=\fr{2\pi}{\gamma}\int\sum\limits_m\left(\fr{a_0}{R}\right)_m e^{im\theta}d\theta\\
    &=\fr{2\pi}{\gamma}\left(\left(\fr{a_0}{R}\right)_0\theta + \sum\limits_{m=1}\fr{-i}{m}\left(\fr{a_0}{R}\right)_m e^{im\theta}\right)\\
    \Theta&=\fr{2\pi}{\gamma}\left(\left(\fr{a_0}{R}\right)_0\theta + \sum\limits_{m=1}\fr{-i}{m}\left(\fr{a_0}{R}\right)_m (e^{im\theta}-1)\right),
\end{align*}
where the constant satisfies $\Theta(\theta=0)=0$.
Since the system is axisymmetric, the values depend only on one variable, and there are many grid points, 
interpolation will be reasonably accurate. Now the coordinate mapping is fully described on an evenly spaced $(\Theta,\zeta)$ grid.
\subsection*{Degeneracy of $s_{\kappa}$, $s_{\delta}$, and $d_rR_0$}
As suggested my Miller, et al., the parameters $s_{\kappa}$, $s_{\delta}$, and $d_rR_0$ have a degeneracy in defining the equilibrium. Citing experience with numerical equilibria, the authors give the following relations of $s_{\kappa}$ and $s_{\delta}$:
\begin{align*}
s_{\kappa} =& \frac{\kappa-\kappa_0}{\kappa}\\
s_{\delta} =& \frac{\delta}{(1-\delta)^{1/2}},
\end{align*}
where $\kappa_0$ is the elongation on axis.
The degeneracy of $d_rR_0$ is much more complicated, relying on the shaping of the surface, and the pressure gradient. Until a more general and usable relationship for $d_rR_0$ is established, $d_rR_0$ will be treated as an independent parameter.
\end{document}
