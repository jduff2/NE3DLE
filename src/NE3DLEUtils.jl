function dθ_kernel(coords::PestGrid,v::Array{Float64})
    fft_plan = get_rfft_plan(coords)
    M = coords.M_theta
    fft_d1_theta_coeffs = iseven(M) ? vcat(im * rfftfreq(M-1, M-1),[0.0+im*0.0]) : im * rfftfreq(M, M)
    return real(fft_plan \ (fft_d1_theta_coeffs.*(fft_plan*v)))
end

function dζ_kernel(coords::PestGrid,v::Array{Float64})
    fft_plan = get_rfft_plan(coords)
    N = coords.N_zeta
    fft_d1_zeta_coeffs = iseven(N) ? coords.N_field_periods * transpose(im * fftfreq(N, N).*(abs.(fftfreq(N, N)).<div(N,2))) : im * coords.N_field_periods * transpose(fftfreq(N,N))
    return real(fft_plan \ (fft_d1_zeta_coeffs.*(fft_plan*v)))
end

function dθ2_kernel(coords::PestGrid,v::Array{Float64})
    fft_plan = get_rfft_plan(coords)
    M = coords.M_theta
    fft_d2_theta_coeffs = (im * rfftfreq(M, M)).^2
    return real(fft_plan \ (fft_d2_theta_coeffs.*(fft_plan*v)))
end

function dζ2_kernel(coords::PestGrid,v::Array{Float64})
    fft_plan = get_rfft_plan(coords)
    N = coords.N_zeta
    fft_d2_zeta_coeffs = (im * coords.N_field_periods * transpose(fftfreq(N, N))).^2
    return real(fft_plan \ (fft_d2_zeta_coeffs.*(fft_plan*v)))
end

function dθdζ_kernel(coords::PestGrid,v::Array{Float64})
    fft_plan = get_rfft_plan(coords)
    M = coords.M_theta
    N = coords.N_zeta
    fft_d2_theta_zeta_coeffs = -rfftfreq(M, M) .* coords.N_field_periods * transpose(fftfreq(N, N))
    return real(fft_plan \ (fft_d2_theta_zeta_coeffs.*(fft_plan*v)))
end

function fourier_derivative(coords::PestGrid{T},v::AbstractArray{T,2},deriv::Symbol) where {T}
    kernel = deriv === :dθ ? dθ_kernel : deriv === :dζ ? dζ_kernel : deriv === :dθ2 ? dθ2_kernel : deriv === :dζ2 ? dζ2_kernel : deriv === :dθdζ ? dθdζ_kernel : nothing
    return kernel(coords,v)
end

function B_dot_grad(surface::NE3DLESurface{T},v::Array{T,2}) where {T}
    return (fourier_derivative(surface.coords,v,:dζ) .+ surface.params.iota.*fourier_derivative(surface.coords,v,:dθ))./surface.metric.sqrtg
end

function B_dot_grad(params::RuntimeParams,coords::PestGrid{T},metric::Metric{T},v::Array{T,2}) where {T}
    return (fourier_derivative(coords,v,:dζ) .+ params.iota.*fourier_derivative(coords,v,:dθ))./metric.sqrtg
end

function B_dot_grad_B(surface::NE3DLESurface{T}) where {T}
    return (fourier_derivative(surface.coords,surface.geom.B_mag,:dζ) .+ surface.params.iota.*fourier_derivative(surface.coords,surface.geom.B_mag,:dθ))./surface.metric.sqrtg
end

function B_dot_grad_B(params::RuntimeParams,coords::PestGrid{T},geom::MagneticGeometry{T},metric::Metric{T}) where {T}
    return (fourier_derivative(coords,geom.B_mag,:dζ) .+ params.iota.*fourier_derivative(coords,geom.B_mag,:dθ))./metric.sqrtg
end

#Note ∮∮√gdθdζ = 1/4π^2
function flux_surface_avg(surface::NE3DLESurface{T},v::AbstractArray{T,2}) where {T}
    fft_plan = get_rfft_plan(surface.coords)
    return real(fft_plan * (v.*surface.metric.sqrtg))[1,1]/real(fft_plan * (surface.metric.sqrtg))[1,1]
end

function flux_surface_avg(coords::PestGrid{T},metric::Metric{T},v::AbstractArray{T,2}) where {T}
    fft_plan = get_rfft_plan(coords)
    return real(fft_plan * (v.*metric.sqrtg))[1,1]/real(fft_plan * (metric.sqrtg))[1,1]
end
