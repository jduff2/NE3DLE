
"""
    compute_field_line_geometry(surface::NE3DLESurface)
    compute_field_line_geometry(params::RuntimeParams,
                                coords::PestGrid{T},
                                metric::Metric{T},
                                geom::MagneticGeometry{T}
                               ) where {T}

Computes integrated shear, dB/dθ, |B|, |∇ψ|, √g, normal and geodesic
curvatures, normal torsion, and local shear along the field line using ζ as the
field line angle. A 2D FFT is taken of the grid quantities and transformed to 1D
using α0 = θ-ιζ. Further details are in the documentation.
"""
function compute_field_line_geometry(params::RuntimeParams,
                                     coords::PestGrid{T},
                                     metric::Metric{T},
                                     geom::MagneticGeometry{T}
                                    ) where {T}
    M = coords.M_theta
    N = coords.N_zeta
    M_pol = div(M, 2)
    N_tor = div(N, 2)
    iota = params.iota
    nz0 = params.parallel_resolution
    alpha_0 = params.field_line_label
    fft_theta_coeffs = rfftfreq(M, M)
    fft_zeta_coeffs = coords.N_field_periods * transpose(fftfreq(N,N))
    fft_d1_zeta_coeffs = iseven(N) ? coords.N_field_periods * transpose(im * fftfreq(N, N).*(abs.(fftfreq(N, N)).<div(N,2))) : im * coords.N_field_periods * transpose(fftfreq(N,N))
    fft_d1_theta_coeffs = iseven(M) ? vcat(im * rfftfreq(M-1, M-1),[0.0+im*0.0]) : im * rfftfreq(M, M)

    integration_modes = @. 1/(iota * fft_d1_theta_coeffs + fft_d1_zeta_coeffs) * (iota * fft_d1_theta_coeffs + fft_d1_zeta_coeffs != 0)
    if iseven(M)
        integration_modes[M_pol+1,:] .= zeros(Complex{T}, N)
    end
    if iseven(N)
        integration_modes[:,N_tor+1] .= zeros(Complex{T}, length(fft_theta_coeffs))
    end

    fl_geom = FieldLine(params, coords)

    eta = fl_geom.eta

    q_int_shear = @. metric.sqrtg * geom.local_shear * (geom.B_mag / geom.grad_psi_mag)^2

    fft_plan = get_rfft_plan(coords)
    fft_q_int_shear = fft_plan * q_int_shear
    fft_int_q_int_shear = integration_modes.*(fft_q_int_shear) 
    fft_q_int_shear_00 = fft_q_int_shear[1,1]
    fft_B_mag = fft_plan * geom.B_mag
    fft_grad_psi_mag = fft_plan * geom.grad_psi_mag
    fft_curv_normal = fft_plan * geom.curv_normal
    fft_curv_geodesic = fft_plan * geom.curv_geodesic
    fft_torsion_normal = fft_plan * geom.torsion_normal
    fft_sqrtg = fft_plan * metric.sqrtg
    fft_local_shear = fft_plan * geom.local_shear
    fft_B_mag_deriv = fft_plan * (metric.sqrtg.*B_dot_grad_B(params,coords,geom,metric))

    for i = 1:nz0
        phase = @. exp(im*(fft_theta_coeffs*alpha_0 + (fft_theta_coeffs*iota + fft_zeta_coeffs)*eta[i]))
        if iseven(M)
            idx = M_pol+1
            phase[idx,:] = @. 0.5*(exp(im*(fft_theta_coeffs[idx]*alpha_0 + (fft_theta_coeffs[idx]*iota + fft_zeta_coeffs)*eta[i])) + exp(im*(-fft_theta_coeffs[idx]*alpha_0 + (-fft_theta_coeffs[idx]*iota + fft_zeta_coeffs)*eta[i])))
        end
        if iseven(N)
            idx = N_tor+1
            phase[:,idx] = @. 0.5*(exp(im*(fft_theta_coeffs*alpha_0 + (fft_theta_coeffs*iota + fft_zeta_coeffs[idx])*eta[i])) + exp(im*(fft_theta_coeffs*alpha_0 + (fft_theta_coeffs*iota - fft_zeta_coeffs[idx])*eta[i])))
        end
        if iseven(M) && iseven(N)
            pol_idx = M_pol+1
            tor_idx = N_tor+1
            phase[pol_idx,tor_idx] = @. 0.5*cos(fft_theta_coeffs[pol_idx]*alpha_0 + fft_theta_coeffs[pol_idx]*iota*eta[i]) * cos(fft_zeta_coeffs[tor_idx]*eta[i])
        end
        
        B_phase = fft_B_mag.*phase
        psi_phase = fft_grad_psi_mag.*phase
        cn_phase = fft_curv_normal.*phase
        cg_phase = fft_curv_geodesic.*phase
        tn_phase = fft_torsion_normal.*phase
        g_phase = fft_sqrtg.*phase
        s_phase = fft_local_shear.*phase
        Bt_phase = fft_B_mag_deriv.*phase
        q_phase = fft_int_q_int_shear.*phase

        fl_geom.B_mag[i] = real(sum(B_phase) + sum(conj.(B_phase[2:end-iseven(M),:])))
        fl_geom.grad_psi_mag[i] = real(sum(psi_phase) + sum(conj.(psi_phase[2:end-iseven(M),:])))
        fl_geom.curv_normal[i] = real(sum(cn_phase) + sum(conj.(cn_phase[2:end-iseven(M),:])))
        fl_geom.curv_geodesic[i] = real(sum(cg_phase) + sum(conj.(cg_phase[2:end-iseven(M),:])))
        fl_geom.torsion_normal[i] = real(sum(tn_phase) + sum(conj.(tn_phase[2:end-iseven(M),:])))
        fl_geom.sqrtg[i] = real(sum(g_phase) + sum(conj.(g_phase[2:end-iseven(M),:])))
        fl_geom.local_shear[i] = real(sum(s_phase) + sum(conj.(s_phase[2:end-iseven(M),:])))
        fl_geom.B_mag_deriv[i] = real(sum(Bt_phase) + sum(conj.(Bt_phase[2:end-iseven(M),:])))
        fl_geom.q_int_shear[i] = real(sum(q_phase) + sum(conj.(q_phase[2:end-iseven(M),:]))) + eta[i]*fft_q_int_shear_00
    end
    
    fl_geom.integrated_shear .= @. fl_geom.grad_psi_mag^2 / fl_geom.B_mag * (fl_geom.q_int_shear)

    return fl_geom
end

function compute_field_line_geometry(surface::NE3DLESurface)
    return compute_field_line_geometry(surface.params, surface.coords, surface.metric, surface.geom)
end

function NE3DLEFieldLine(surface::NE3DLESurface{T}) where {T}
    field_line = compute_field_line_geometry(surface)
    gene = GeneGeom(surface.params, field_line, surface.norms)
    return NE3DLEFieldLine{T}(surface.params, surface.norms, field_line, gene)
end
# Diagnostic functions
#=
function compute_field_line_geometry2(surface::NE3DLESurface)
    return compute_field_line_geometry2(surface.params, surface.coords, surface.metric, surface.geom)
end

function NE3DLEFieldLine2(surface::NE3DLESurface{T}) where {T}
    field_line = compute_field_line_geometry2(surface)
    gene = GeneGeom(surface.params, field_line, surface.norms)
    return NE3DLEFieldLine{T}(surface.params, surface.norms, field_line, gene)
end

function compute_field_line_geometry2(params::RuntimeParams, coords::PestGrid, metric::Metric, geom::MagneticGeometry)
    M = coords.M_theta
    N = coords.N_zeta
    iota = params.iota
    nz0 = params.parallel_resolution
    alpha_0 = -params.field_line_label

	fft_theta_coeffs = iseven(M) ? vcat(rfftfreq(M-1, M-1),[0.0+im*0.0]) : rfftfreq(M, M)
	fft_zeta_coeffs = iseven(N) ? coords.N_field_periods * transpose(fftfreq(N, N).*(abs.(fftfreq(N, N)).<div(N,2))) : coords.N_field_periods * transpose(fftfreq(N,N))
	fft_d1_theta_coeffs = iseven(M) ? vcat(im * rfftfreq(M-1, M-1),[0.0+im*0.0]) : im * rfftfreq(M, M)
	integration_modes = @. -im/(iota * fft_theta_coeffs + fft_zeta_coeffs) * (iota * fft_theta_coeffs + fft_zeta_coeffs != 0)
	if iseven(M)
		integration_modes[div(M,2)+1,:] .= zeros(ComplexF64,N)
	end
	if iseven(N)
		integration_modes[:,div(N,2)+1] .= zeros(ComplexF64,length(fft_theta_coeffs))
	end

    fl_geom = FieldLine(params,coords)

    eta = fl_geom.eta

    q_int_shear = metric.sqrtg .* geom.local_shear .* (geom.B_mag ./ geom.grad_psi_mag).^2

    fft_plan = get_rfft_plan(coords)
    fft_int_q_int_shear = integration_modes.*(fft_plan * q_int_shear)
	fft_q_int_shear_00 = (fft_plan * q_int_shear)[1,1]
    fft_B_mag = fft_plan * geom.B_mag
    fft_grad_psi_mag = fft_plan * geom.grad_psi_mag
    fft_curv_normal = fft_plan * geom.curv_normal
    fft_curv_geodesic = fft_plan * geom.curv_geodesic
    fft_torsion_normal = fft_plan * geom.torsion_normal
    fft_sqrtg = fft_plan * metric.sqrtg
    fft_local_shear = fft_plan * geom.local_shear
	fft_B_mag_deriv = @. fft_d1_theta_coeffs*fft_B_mag

    for i in 1:params.parallel_resolution
		phase = @. exp(im*(fft_theta_coeffs*alpha_0 + (fft_theta_coeffs*iota + fft_zeta_coeffs)*eta[i]))
		if iseven(M)
			idx = div(M,2)+1
			phase[idx,:] = @. 0.5*(exp(im*(fft_theta_coeffs[idx]*alpha_0 + (fft_theta_coeffs[idx]*iota + fft_zeta_coeffs)*eta[i])) + exp(im*(-fft_theta_coeffs[idx]*alpha_0 + (-fft_theta_coeffs[idx]*iota + fft_zeta_coeffs)*eta[i])))
		end
		if iseven(N)
			idx = div(N,2)+1
			phase[:,idx] = @. 0.5*(exp(im*(fft_theta_coeffs*alpha_0 + (fft_theta_coeffs*iota + fft_zeta_coeffs[idx])*eta[i])) + exp(im*(fft_theta_coeffs*alpha_0 + (fft_theta_coeffs*iota - fft_zeta_coeffs[idx])*eta[i])))
		end
		if iseven(M) && iseven(N)
			pol_idx = div(M,2)+1
			tor_idx = div(N,2)+1
			phase[pol_idx,tor_idx] = @. 0.5*(cos(fft_theta_coeffs[pol_idx]*alpha_0 + (fft_theta_coeffs[pol_idx]*iota + fft_zeta_coeffs[tor_idx])*eta[i]) * cos(fft_theta_coeffs[pol_idx]*alpha_0 + (fft_theta_coeffs[pol_idx]*iota - fft_zeta_coeffs[tor_idx])*eta[i]))
		end
		fl_geom.B_mag[i] = real(sum(fft_B_mag[1,:] .* phase[1,:]) + sum(fft_B_mag[2:end,:].*phase[2:end,:] .+ conj.(fft_B_mag[2:end,:].*phase[2:end,:])))
		fl_geom.grad_psi_mag[i] = real(sum(fft_grad_psi_mag[1,:] .* phase[1,:]) + sum(fft_grad_psi_mag[2:end,:].*phase[2:end,:] .+ conj.(fft_grad_psi_mag[2:end,:].*phase[2:end,:])))
		fl_geom.curv_normal[i] = real(sum(fft_curv_normal[1,:] .* phase[1,:]) + sum(fft_curv_normal[2:end,:].*phase[2:end,:] .+ conj.(fft_curv_normal[2:end,:].*phase[2:end,:])))
		fl_geom.curv_geodesic[i] = real(sum(fft_curv_geodesic[1,:] .* phase[1,:]) + sum(fft_curv_geodesic[2:end,:].*phase[2:end,:] .+ conj.(fft_curv_geodesic[2:end,:].*phase[2:end,:])))
		fl_geom.torsion_normal[i] = real(sum(fft_torsion_normal[1,:] .* phase[1,:]) + sum(fft_torsion_normal[2:end,:].*phase[2:end,:] .+ conj.(fft_torsion_normal[2:end,:].*phase[2:end,:])))
		fl_geom.sqrtg[i] = real(sum(fft_sqrtg[1,:] .* phase[1,:]) + sum(fft_sqrtg[2:end,:].*phase[2:end,:] .+ conj.(fft_sqrtg[2:end,:].*phase[2:end,:])))
		fl_geom.local_shear[i] = real(sum(fft_local_shear[1,:] .* phase[1,:]) + sum(fft_local_shear[2:end,:].*phase[2:end,:] .+ conj.(fft_local_shear[2:end,:].*phase[2:end,:])))
		fl_geom.B_mag_deriv[i] = real(sum(fft_B_mag_deriv[1,:] .* phase[1,:]) + sum(fft_B_mag_deriv[2:end,:].*phase[2:end,:] .+ conj.(fft_B_mag_deriv[2:end,:].*phase[2:end,:])))
		fl_geom.q_int_shear[i] = real(sum(fft_int_q_int_shear[1,:] .* phase[1,:]) + sum(fft_int_q_int_shear[2:end,:].*phase[2:end,:] .+ conj.(fft_int_q_int_shear[2:end,:].*phase[2:end,:]))) + eta[i]*fft_q_int_shear_00
    end
    fl_geom.integrated_shear .= @. fl_geom.grad_psi_mag^2 / fl_geom.B_mag * (fl_geom.q_int_shear)

    return fl_geom
end

=#