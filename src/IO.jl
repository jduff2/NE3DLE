#Prevents overwriting existing data by checking
# if the output path given in input.txt already exists, then
# making a new directory with _# appended
function check_directory_exist!(out_path::String)
    dir_check = isdir("$(out_path)")
    num_dir = 0

    while dir_check == true
        num_dir = num_dir + 1
        dir_name = split("$out_path", "_")[1]
        dir_name = String("$dir_name")
        out_path = "$(dir_name)_$(num_dir)"
        dir_check = isdir("$(out_path)")
    end

    mkdir(out_path)

    return out_path
end

#To see how many variables are trying to be scanned. Will only allow for one or
# zero scans.
function check_parameter_scan(par_dict::Dict{String,Any},out_params::OutputParams)
    n_scan_parameters = 0
    scan_var = Dict()
    n_scan_runs = 1
    for par_key in collect(keys(par_dict))
        if length(par_dict[par_key]) > 1 && typeof(par_dict[par_key]) != String
            n_scan_parameters += 1
            scan_var[par_key] = par_dict[par_key]
            n_scan_runs = length(par_dict[par_key])
        end
    end

    @assert n_scan_parameters <= 1 "Only one parameter may be scanned at a time. Input file trys to scan $(keys(out_params.scan_var))."

    return n_scan_parameters, n_scan_runs, scan_var
end

#Creates head output directory and individual directories for each variable
# changed in scan, then writes input files in each output directory.
function write_param_file_scan(par_dict::Dict{String,Any}, out_params::OutputParams)
    pfile = "NE3DLE.in"
    scanKey = collect(keys(out_params.scan_var))[1]
    par_file = Array{String}(undef,length(out_params.scan_var[scanKey]))
    out_path = Array{String}(undef,length(out_params.scan_var[scanKey]))
    scan_path = string(out_params.parameter_output_path[1],"$(scanKey)_scan")

    scan_path = check_directory_exist!(scan_path)

    for m in 1:out_params.n_scan_runs
        out_path[m] = string("$(scan_path)/","$(scanKey)","_$(m)/")
        mkdir("$(out_path[m])")
        par_file[m] = "$(out_path[m])$(pfile)"
        open("$(out_path[m])$(pfile)", "w") do out_par_f
            for par_key in collect(keys(par_dict))
                if length(par_dict[par_key]) > 1 && typeof(par_dict[par_key]) != String && !(typeof(par_dict[par_key]) <: AbstractDict)
                    write(out_par_f, "$par_key = $(par_dict[par_key][m])\n")
                elseif !(typeof(par_dict[par_key]) <: AbstractDict)
                    write(out_par_f, "$par_key = $(par_dict[par_key])\n")
                end
            end
        end
    end
    return out_path
end

#Writes input file to output directory for a non-scan case.
function write_param_file(input_file::String,
                          input_location::String,
                          out_params::OutputParams)
    out_path = out_params.parameter_output_path[1]
    par_file = "$(out_path)/$(input_file)"
    cp(input_location, "$(par_file)"; follow_symlinks=false)

    return nothing
end

#Creates the output file for GENE parameters.
function write_NE3DLE_out_file(out_params::OutputParams,
                               params::RuntimeParams,
                               gene::GeneGeom{T},
                               out_path::String
                              ) where {T}
    #Angle coordinate in units of π. Not read by GENE, but is printed for plotting purposes
    η = params.fl_coordinate == "zeta" ? (gene.θ*pi .- gene.alpha_0)/gene.iota/pi : gene.θ

    fname = "NE3DLE_out.txt"
    open("$(out_path)$(fname)", "w") do f
        write(f, "&parameters\n")
        write(f, Printf.@sprintf "s0 = %1.7f\n" gene.s0)
        write(f, Printf.@sprintf "!alpha0 = %1.7f\n" gene.alpha_0)
        write(f, Printf.@sprintf "!major_R = %1.7f\n" gene.major_R)
        write(f, Printf.@sprintf "!minor_r = %1.7f\n" gene.minor_a)
        write(f, Printf.@sprintf "Bref = %1.7f\n" gene.B_ref)
        write(f, Printf.@sprintf "my_dpdx = %1.7f\n" gene.my_dpdx)
        write(f, Printf.@sprintf "q0 = %1.7f\n" 1/gene.iota)
        write(f, Printf.@sprintf "shat = %2.7f\n" gene.shat)
        write(f, Printf.@sprintf "gridpoints = %d\n" gene.gridpoints)
        write(f, Printf.@sprintf "n_pol = %d\n" gene.n_pol)
        if params.mapping == "miller"
            write(f, Printf.@sprintf "Cy = %1.7f\n" gene.Cy)
        end

        #write(f, Printf.@sprintf "nfp = %d\n" params.nfp)
        write(f, "/\n")

        for m in 1:gene.gridpoints
            write(f, Printf.@sprintf "%1.7e    %1.7e    %1.7e    %1.7e    %1.7e    %1.7e    %1.7e    %1.7e    %1.7e\n" gene.gxx[m] gene.gxy[m] gene.gyy[m] gene.Bhat[m] gene.jac[m] gene.L2[m] gene.L1[m] gene.∂B∂θ[m] η[m])
        end
    end

    return nothing
end

function write_NE3DLE_out_file(filename::String,
                               params::RuntimeParams,
                               gene::GeneGeom{T};
                               write_eta::Bool=true
                              ) where {T}
    #Angle coordinate in units of π. Not read by GENE, but is printed for plotting purposes
    η = params.fl_coordinate == "zeta" ? (gene.θ*pi .- gene.alpha_0)/gene.iota/pi : gene.θ

    open(filename, "w") do f
        write(f, "&parameters\n")
        write(f, Printf.@sprintf "s0 = %1.7f\n" gene.s0)
        write(f, Printf.@sprintf "!alpha0 = %1.7f\n" gene.alpha_0)
        write(f, Printf.@sprintf "!major_R = %1.7f\n" gene.major_R)
        write(f, Printf.@sprintf "!minor_r = %1.7f\n" gene.minor_a)
        write(f, Printf.@sprintf "Bref = %1.7f\n" gene.B_ref)
        write(f, Printf.@sprintf "my_dpdx = %1.7f\n" gene.my_dpdx)
        write(f, Printf.@sprintf "q0 = %1.7f\n" 1/gene.iota)
        write(f, Printf.@sprintf "shat = %2.7f\n" gene.shat)
        write(f, Printf.@sprintf "gridpoints = %d\n" gene.gridpoints)
        write(f, Printf.@sprintf "n_pol = %d\n" gene.n_pol)
        if params.mapping == "miller"
            write(f, Printf.@sprintf "Cy = %1.7f\n" gene.Cy)
        end

        #write(f, Printf.@sprintf "nfp = %d\n" params.nfp)
        write(f, "/\n")
        if write_eta == true
            for m in 1:gene.gridpoints
                write(f, Printf.@sprintf "\t%1.7e\t%1.7e\t%1.7e\t%1.7e\t%1.7e\t%1.7e\t%1.7e\t%1.7e\t%1.7e\n" gene.gxx[m] gene.gxy[m] gene.gyy[m] gene.Bhat[m] gene.jac[m] gene.L2[m] gene.L1[m] gene.∂B∂θ[m] η[m])
            end
        else
            for m in 1:gene.gridpoints
                write(f, Printf.@sprintf "\t%1.7e\t%1.7e\t%1.7e\t%1.7e\t%1.7e\t%1.7e\t%1.7e\t%1.7e\n" gene.gxx[m] gene.gxy[m] gene.gyy[m] gene.Bhat[m] gene.jac[m] gene.L2[m] gene.L1[m] gene.∂B∂θ[m])
            end
        end
    end

    return nothing
end

"""
   convert_array_from_string_1D!(string_in::String,
                             T::Type,
                             vals::Array;
                             file_type::String="parameters")

Converts a string into a 1D array specified by type T.  The user is responsible for ensuring the
correct type.  The string can be comprised of the following formats:
* a comma separated string, e.g. "1,2,3" => [1,2,3]
* a whitespace separated string, e.g. "1 2 3" => [1,2,3]

The following functionality is useful for reading simulations with checkpoints
* a string with "--" where a range of values are inserted between the "--"
 * the values inserted can be integers "13--17" => [13,14,15,16,17] or
 * the values can be characters "15a--15g" => ["15a","15b","15c","15d","15e","15f","15g"]
* a string with "=" as the last character denotes all of the subsequent run IDs in
   the data directory set in diag.data_path.  The number of IDs search is
   set by diag.ID_search_int (default = 100).  The same can be done with characters
   with diag.ID_search_char (default = 'z')

The function is recursive and will act on substrings until no new substrings are found.
The resulting data is pushed onto the array designated by `vals`.  The user must ensure vals
is an empty array when the routine is intially called.
"""
function convert_array_from_string_1D!(string_in::String,
                                   T::Type,
                                   vals::Array;
                                   file_type::String="parameters")
  # Check for the existence of certain characters, namely the "=", "--", " " and "," characters
  # used to indicate a continuation, range, and delimited values, respectively
    if occursin(",",string_in)
        dlm = '='
    end
    if occursin("--",string_in)
        dlm = "--"
    end
    if occursin(" ",string_in)
        dlm = ' '
    end
    if occursin(",",string_in)
        dlm = ','
    end

  # If no delimiter has been found, then the string contains only one entry
    if @isdefined dlm
        split_arr = split(string_in,dlm)
        # Functions are involved in reading parameters files
        if dlm == ','
            for s in split_arr
                # Empty values will not be pushed
                ~isempty(s) ? convert_array_from_string_1D!(s,T,vals) : nothing
            end
        elseif dlm == ' '
            for s in split_arr
                ~isempty(s) ? convert_array_from_string_1D!(s,T,vals) : nothing
            end

        # Start of functions involved in setting the diagnostic state, such as filling a range
        # of values that can designate the continuation of simulations
        elseif dlm == "--"
            # Determine if the last character is an integer or a character
            last_char = split_arr[1][length(split_arr[1])]
            temp_type = Any
            try parse(Int,last_char)
                temp_type = Int64
            catch
                # Anything not a an integer is a char
                temp_type = Char
            end
            if temp_type <: Integer
                l_zeros = match(r"0*",split_arr[1]).match
                # Determine the integer range
                i_low = parse(temp_type,convert(String,split_arr[1]))
                i_high = parse(temp_type,convert(String,split_arr[2]))
                for i = i_low:i_high
                    string_i = isempty(l_zeros) ? string(i) : lpad(i,length(split_arr[1]),'0')
                    convert_array_from_string_1D!(string_i,T,vals)
                end
            elseif temp_type <: Char
                # Check to make sure the base values for the runs are the same
                base1 = convert(String,split_arr[1][1:length(split_arr[1])-1])
                base2 = convert(String,split_arr[2][1:length(split_arr[2])-1])
                base1 == base2 ? nothing : throw(ArgumentError("$base2 must be the same value as $base1"))
                # Determine the character range (must be in alphabetical order)
                last_char2 = split_arr[2][length(split_arr[2])]
                for i in last_char:last_char2
                    convert_array_from_string_1D!(base1*i,String,vals)
                end
            end
        # Provide a fill if the user is lazy, the fill is determined by diag.ID_search_int
        # or diag.ID_search_char
        elseif dlm == '='
            # Determine if the last char is an int
            last_char = split_arr[1][length(split_arr[1])]
            temp_type = Any
            try parse(Int,last_char)
                temp_type = Int64
            catch
                temp_type = Char
            end
            base = convert(String,split_arr[1][1:length(split_arr[1])-1])
            data_path = diag.data_path
            if temp_type <: Integer
                base_int = parse(Int,convert(String,split_arr[1]))
                max_int += diag.ID_search_int
                par_files = readdir(data_path)
                for i in base_int:max_int
                    file = data_path*"/"*file_type*"_"*base*i
                    isfile(file) ? convert_array_from_string_1D!(base*i,T,vals) : nothing
                end
            elseif temp_type <: Char
                for c in last_char:base_diag.ID_search_char
                    file = data_path*"/"*file_type*"_"*base*c
                    isfile(file) ? convert_array_from_string_1D!(base*i,String,vals) : nothing
                end
            end
        end
    else
        if T != String
            if T != Bool
                push!(vals,parse(T,string_in))
            else
            # Need to deal with Fortran values of bools
                if occursin(r"[Tt1]",string_in)
                    push!(vals,parse(T,"true"))
                end
                if occursin(r"[Ff0]",string_in)
                    push!(vals,parse(T,"false"))
                end
            end
        else
            # Strip the ' literal from Fortran strings
            string_in = replace(string_in,"'"=>"")
            push!(vals,string_in)
        end
    end
    return nothing
end

function convert_array_from_string_1D!(string_in::SubString{String},
                                   T::Type,
                                   vals::Array;
                                   file_type::String="parameters")
    string_in = convert(String,string_in)
    convert_array_from_string_1D!(string_in,T,vals)
    return nothing
end

"""
   read_par_file(file_path::String)

Reads a parameters file and returns a dictionary with all the parameter
names as keywords and corresponding parameter values in array form.  The
parameter values have the type determined by types_dict set in type_defs.jl.
For a simulation with `n_spec = x`, there are `x` entries in each of the
species keywords.
"""
function read_par_file(file_path::String)
    par_stream = open(file_path)
    par_file = readlines(par_stream)
    close(par_stream)

    n_par_lines = length(par_file)
    par_strings = Array{String}[]
    key_list = Array{String}(undef,0)
    par_dict = Dict{String,Any}()

    # Split the parameter files lines by their key and value, push to
    # a string array holding the key and value in string representation
    for i = 1:n_par_lines
        par_split = split(par_file[i],"=")
        if length(par_split) == 2
            par_key = strip(par_split[1])
            par_val = strip(par_split[2])
            push!(par_strings,[par_key,par_val])
            push!(key_list,par_key)
        end
    end

    # Create a dictionary to hold the value of each occurance of a keyword
    # This is necessary because multiple species have the same keyword, the
    # value associated with each keyword is an array of positions in the
    # par_strings array
    key_count = Dict{String,Array{Int64}}()
    pos = 1
    for key in key_list
        if haskey(key_count,key)
            temp_arr = key_count[key]
            temp_arr[1] += 1
            push!(temp_arr,pos)
            key_count[key] = temp_arr
        else
            key_count[key] = [1, pos]
        end
        pos += 1
    end

    # Construct a single string for each value, which may be comprised
    # of multiple strings.  This is necessary to use convert_array_from_string_1D!
    for key in keys(key_count)
        # TODO This is a temporary fix to deal with a complex entry
        if key != "taumfn"
            n_keys = key_count[key][1]
            key_inds = key_count[key][2:n_keys+1]
            if n_keys == 1
                key_ind = key_inds[1]
                par_val = par_strings[key_ind][2]
            else
                if key != "lx"
                    for i=1:n_keys
                        key_ind = key_inds[i]
                        if i == 1
                            par_val = par_strings[key_ind][2]
                        else
                            par_val *= ","*par_strings[key_ind][2]
                        end
                    end
                else
                    key_ind = key_inds[n_keys]
                    par_val = par_strings[key_ind][2]
                end
            end
            par_dict[key] = par_val
        end
    end

    # use convert_array_from_string_1D! to sanitize the input
    for key in keys(par_dict)
        T = types_dict[key]
        vals = Array{T}(undef,0)
        convert_array_from_string_1D!(par_dict[key],types_dict[key],vals)
        par_dict[key] = length(vals) == 1 ? vals[1] : vals
    end
    if !haskey(par_dict,"Rmn")
        par_dict["Rmn"] = Dict{Tuple{Int,Int},types_dict["iota"]}()
    end
    if !haskey(par_dict,"Zmn")
        par_dict["Zmn"] = Dict{Tuple{Int,Int},types_dict["iota"]}()
    end
    return par_dict
end

function swap_input_variables(file::String,current::String,change::String)
    par_stream = open(file)
    par_file = readlines(par_stream)
    close(par_stream)
    n_par_lines = length(par_file)
    for i = 1:n_par_lines
        if occursin(current,par_file[i])
            par_file[i] = change
        end
    end
    open(file,"w") do f
        for i in 1:n_par_lines
            write(f,"$(par_file[i])\n")
        end
    end
end

function write_surface_to_hdf5_file(surface::NE3DLESurface{T};fname::String="./test_surface.hdf5") where {T}
    fid = h5open(fname, "cw")
    create_group(fid, "NE3DLESurface")
    create_group(fid, "NE3DLESurface/coords")
    create_group(fid, "NE3DLESurface/params")
    create_group(fid, "NE3DLESurface/norms")
    create_group(fid, "NE3DLESurface/imap")
    create_group(fid, "NE3DLESurface/metric")
    create_group(fid, "NE3DLESurface/geom")

    fid["NE3DLESurface/coords"]["M_theta"] = surface.coords.M_theta
    fid["NE3DLESurface/coords"]["N_zeta"] = surface.coords.N_zeta
    fid["NE3DLESurface/coords"]["N_field_periods"] = surface.coords.N_field_periods
    fid["NE3DLESurface/coords"]["theta"] = surface.coords.theta
    fid["NE3DLESurface/coords"]["zeta"] = surface.coords.zeta

    fid["NE3DLESurface/params"]["M_theta"] = surface.params.M_theta
    fid["NE3DLESurface/params"]["N_zeta"] = surface.params.N_zeta
    fid["NE3DLESurface/params"]["iota"] = surface.params.iota
    fid["NE3DLESurface/params"]["parallel_resolution"] = surface.params.parallel_resolution
    fid["NE3DLESurface/params"]["computed_field_periods"] = surface.params.computed_field_periods
    fid["NE3DLESurface/params"]["field_line_label"] = surface.params.field_line_label
    fid["NE3DLESurface/params"]["grad_iota"] = surface.params.surface_quant["grad_iota"]
    fid["NE3DLESurface/params"]["grad_p"] = surface.params.surface_quant["grad_p"]
    fid["NE3DLESurface/params"]["surface_current"] = surface.params.surface_quant["surface_current"]
    fid["NE3DLESurface/params"]["mapping"] = surface.params.mapping
    fid["NE3DLESurface/params"]["debug_flag"] = surface.params.debug_flag
    fid["NE3DLESurface/params"]["fl_coordinate"] = surface.params.fl_coordinate
    fid["NE3DLESurface/params"]["nfp"] = surface.params.nfp

    fid["NE3DLESurface/norms"]["R0"] = surface.norms.R0
    fid["NE3DLESurface/norms"]["ρ"] = surface.norms.ρ
    fid["NE3DLESurface/norms"]["dpsi_drho"] = surface.norms.dpsi_drho
    fid["NE3DLESurface/norms"]["Vp"] = surface.norms.Vp
    fid["NE3DLESurface/norms"]["B_0"] = surface.norms.B_0
    fid["NE3DLESurface/norms"]["mu_0"] = surface.norms.mu_0
    fid["NE3DLESurface/norms"]["L_ref"] = surface.norms.L_ref
    fid["NE3DLESurface/norms"]["B_ref"] = surface.norms.B_ref

    fid["NE3DLESurface/imap"]["R"] = surface.imap.R
    fid["NE3DLESurface/imap"]["R_psi"] = surface.imap.R_psi
    fid["NE3DLESurface/imap"]["R_theta"] = surface.imap.R_theta
    fid["NE3DLESurface/imap"]["R_zeta"] = surface.imap.R_zeta
    fid["NE3DLESurface/imap"]["R_psi_theta"] = surface.imap.R_psi_theta
    fid["NE3DLESurface/imap"]["R_psi_zeta"] = surface.imap.R_psi_zeta
    fid["NE3DLESurface/imap"]["R_theta_theta"] = surface.imap.R_theta_theta
    fid["NE3DLESurface/imap"]["R_theta_zeta"] = surface.imap.R_theta_zeta
    fid["NE3DLESurface/imap"]["R_zeta_zeta"] = surface.imap.R_zeta_zeta
    fid["NE3DLESurface/imap"]["Z"] = surface.imap.Z
    fid["NE3DLESurface/imap"]["Z_psi"] = surface.imap.Z_psi
    fid["NE3DLESurface/imap"]["Z_theta"] = surface.imap.Z_theta
    fid["NE3DLESurface/imap"]["Z_zeta"] = surface.imap.Z_zeta
    fid["NE3DLESurface/imap"]["Z_psi_theta"] = surface.imap.Z_psi_theta
    fid["NE3DLESurface/imap"]["Z_psi_zeta"] = surface.imap.Z_psi_zeta
    fid["NE3DLESurface/imap"]["Z_theta_theta"] = surface.imap.Z_theta_theta
    fid["NE3DLESurface/imap"]["Z_theta_zeta"] = surface.imap.Z_theta_zeta
    fid["NE3DLESurface/imap"]["Z_zeta_zeta"] = surface.imap.Z_zeta_zeta
    fid["NE3DLESurface/imap"]["Phi"] = surface.imap.Phi

    fid["NE3DLESurface/metric"]["sqrtg"] = surface.metric.sqrtg
    fid["NE3DLESurface/metric"]["sqrtg_psi"] = surface.metric.sqrtg_psi
    fid["NE3DLESurface/metric"]["g_theta_theta"] = surface.metric.g_theta_theta
    fid["NE3DLESurface/metric"]["g_theta_zeta"] = surface.metric.g_theta_zeta
    fid["NE3DLESurface/metric"]["g_zeta_zeta"] = surface.metric.g_zeta_zeta

    fid["NE3DLESurface/geom"]["B_mag"] = surface.geom.B_mag
    fid["NE3DLESurface/geom"]["grad_psi_mag"] = surface.geom.grad_psi_mag
    fid["NE3DLESurface/geom"]["curv_normal"] = surface.geom.curv_normal
    fid["NE3DLESurface/geom"]["curv_geodesic"] = surface.geom.curv_geodesic
    fid["NE3DLESurface/geom"]["torsion_normal"] = surface.geom.torsion_normal
    fid["NE3DLESurface/geom"]["local_shear"] = surface.geom.local_shear
    fid["NE3DLESurface/geom"]["lambda_PS"] = surface.geom.lambda_PS
    fid["NE3DLESurface/geom"]["D_coeff"] = surface.geom.D_coeff
    fid["NE3DLESurface/geom"]["h_coeff"] = surface.geom.h_coeff
    fid["NE3DLESurface/geom"]["B_mag_psi"] = surface.geom.B_mag_psi

    close(fid)
end

function write_input_to_hdf5_file(par_dict::AbstractDict;fname::String="./test_dict.hdf5") 
    fid = h5open(fname, "cw")

    gid = create_group(fid,"par_dict")
    for (key,value) in par_dict
        if key == "Rmn" || key == "Zmn"
            if length(keys(value)) == 0
                gid[key] = "Dict{Tuple{Int64, Int64}, Float64}()"
            else
                for (mode,amplitude) in value
                    gid["$key$mode"] = amplitude
                end
            end
        else
            gid[key] = value
        end
    end

    close(fid)
end

function write_input_and_surface_to_hdf5_file(par_dict::AbstractDict,surface::NE3DLESurface{T};fname::String="./test.hdf5") where {T}
    write_input_to_hdf5_file(par_dict,fname=fname)
    write_surface_to_hdf5_file(surface,fname=fname)
end

function read_input_from_hdf5_file(fname::String)
    function parsetuple(s::AbstractString)
        return Tuple(parse.(Int, split(s, ",")))
    end
    par_dict = Dict{String,Any}()
    fid = h5open(fname, "r")
    haskey(fid,"par_dict") || "This file does not contain an input dictionary."
    for key in keys(fid["par_dict"])
        value = read(fid["par_dict"][key])
        if occursin("Rmn", key) || occursin("Zmn", key)
            if occursin("(",key)
                var, modes = split(key, "(")
                modes = split(String(modes), ")")[1]
                modes = parsetuple(modes)
                if haskey(par_dict, var)
                    par_dict[var][modes] = value
                else
                    par_dict[var] = Dict{Tuple{Int, Int}, Float64}()
                    par_dict[var][modes] = value
                end
            else
                par_dict[key] = Dict{Tuple{Int64, Int64}, Float64}()
            end
        else
            par_dict[key] = value
        end
    end
    close(fid)
    return par_dict
end

function read_PestGrid_from_hdf5_file(fname::String)
    fid = h5open(fname, "r")
    haskey(fid, "NE3DLESurface") || "This file does not contain a NE3DLESurface."
    haskey(fid, "NE3DLESurface/coords") || "This file does not contain a PestGrid constructor."
    coords = PestGrid(read(fid["NE3DLESurface/coords"]["M_theta"]),read(fid["NE3DLESurface/coords"]["N_zeta"]),read(fid["NE3DLESurface/coords"]["N_field_periods"]))
    close(fid)
    return coords
end
   
function read_RuntimeParams_from_hdf5_file(fname::String)
    fid = h5open(fname, "r")
    haskey(fid, "NE3DLESurface") || "This file does not contain a NE3DLESurface."
    haskey(fid, "NE3DLESurface/params") || "This file does not contain a RuntimeParams constructor."
    surface_quant = Dict("grad_iota"=>read(fid["NE3DLESurface/params"]["grad_iota"]),"grad_p"=>read(fid["NE3DLESurface/params"]["grad_p"]),"surface_current"=>read(fid["NE3DLESurface/params"]["surface_current"]))
    params = RuntimeParams(read(fid["NE3DLESurface/params"]["M_theta"]),
                           read(fid["NE3DLESurface/params"]["N_zeta"]),
                           read(fid["NE3DLESurface/params"]["iota"]),
                           read(fid["NE3DLESurface/params"]["parallel_resolution"]),
                           read(fid["NE3DLESurface/params"]["computed_field_periods"]),
                           read(fid["NE3DLESurface/params"]["field_line_label"]),
                           surface_quant,
                           read(fid["NE3DLESurface/params"]["mapping"]),
                           read(fid["NE3DLESurface/params"]["debug_flag"]),
                           read(fid["NE3DLESurface/params"]["fl_coordinate"]),
                           read(fid["NE3DLESurface/params"]["nfp"]))

    close(fid)
    return params
end

function read_NormalizingParams_from_hdf5_file(fname::String)
    fid = h5open(fname, "r")
    haskey(fid, "NE3DLESurface") || "This file does not contain a NE3DLESurface."
    haskey(fid, "NE3DLESurface/norms") || "This file does not contain a NormalizingParams constructor."
    norms = NormalizingParams(read(fid["NE3DLESurface/norms"]["R0"]),
                              read(fid["NE3DLESurface/norms"]["ρ"]),
                              read(fid["NE3DLESurface/norms"]["dpsi_drho"]),
                              read(fid["NE3DLESurface/norms"]["Vp"]),
                              read(fid["NE3DLESurface/norms"]["B_0"]),
                              read(fid["NE3DLESurface/norms"]["mu_0"]),
                              read(fid["NE3DLESurface/norms"]["L_ref"]),
                              read(fid["NE3DLESurface/norms"]["B_ref"]))
    
    
    close(fid)
    return norms
end

function read_InverseMap_from_hdf5_file(fname::String)
    fid = h5open(fname, "r")
    haskey(fid, "NE3DLESurface") || "This file does not contain a NE3DLESurface."
    haskey(fid, "NE3DLESurface/imap") || "This file does not contain a InverseMap constructor."
    imap = InverseMap(read(fid["NE3DLESurface/imap"]["R"]), 
                      read(fid["NE3DLESurface/imap"]["R_psi"]),
                      read(fid["NE3DLESurface/imap"]["R_theta"]),
                      read(fid["NE3DLESurface/imap"]["R_zeta"]),
                      read(fid["NE3DLESurface/imap"]["R_psi_theta"]),
                      read(fid["NE3DLESurface/imap"]["R_psi_zeta"]),
                      read(fid["NE3DLESurface/imap"]["R_theta_theta"]),
                      read(fid["NE3DLESurface/imap"]["R_theta_zeta"]),
                      read(fid["NE3DLESurface/imap"]["R_zeta_zeta"]),
                      read(fid["NE3DLESurface/imap"]["Z"]),
                      read(fid["NE3DLESurface/imap"]["Z_psi"]),
                      read(fid["NE3DLESurface/imap"]["Z_theta"]),
                      read(fid["NE3DLESurface/imap"]["Z_zeta"]),
                      read(fid["NE3DLESurface/imap"]["Z_psi_theta"]),
                      read(fid["NE3DLESurface/imap"]["Z_psi_zeta"]),
                      read(fid["NE3DLESurface/imap"]["Z_theta_theta"]),
                      read(fid["NE3DLESurface/imap"]["Z_theta_zeta"]),
                      read(fid["NE3DLESurface/imap"]["Z_zeta_zeta"]),
                      read(fid["NE3DLESurface/imap"]["Phi"]))

    close(fid)
    return imap
end

function read_Metric_from_hdf5_file(fname::String)
    fid = h5open(fname, "r")
    haskey(fid, "NE3DLESurface") || "This file does not contain a NE3DLESurface."
    haskey(fid, "NE3DLESurface/metric") || "This file does not contain a Metric constructor."
    metric = Metric(read(fid["NE3DLESurface/metric"]["sqrtg"]),
                    read(fid["NE3DLESurface/metric"]["sqrtg_psi"]),
                    read(fid["NE3DLESurface/metric"]["g_theta_theta"]),
                    read(fid["NE3DLESurface/metric"]["g_theta_zeta"]),
                    read(fid["NE3DLESurface/metric"]["g_zeta_zeta"]))
    
    close(fid)
    return metric
end

function read_MagneticGeometry_from_hdf5_file(fname::String)
    fid = h5open(fname, "r")
    haskey(fid, "NE3DLESurface") || "This file does not contain a NE3DLESurface."
    haskey(fid, "NE3DLESurface/geom") || "This file does not contain a MagneticGeometry constructor."
    geom = MagneticGeometry(read(fid["NE3DLESurface/geom"]["B_mag"]),
                            read(fid["NE3DLESurface/geom"]["grad_psi_mag"]),
                            read(fid["NE3DLESurface/geom"]["curv_normal"]),
                            read(fid["NE3DLESurface/geom"]["curv_geodesic"]),
                            read(fid["NE3DLESurface/geom"]["torsion_normal"]),
                            read(fid["NE3DLESurface/geom"]["local_shear"]),
                            read(fid["NE3DLESurface/geom"]["lambda_PS"]),
                            read(fid["NE3DLESurface/geom"]["D_coeff"]),
                            read(fid["NE3DLESurface/geom"]["h_coeff"]),
                            read(fid["NE3DLESurface/geom"]["B_mag_psi"]))

    close(fid)
    return geom
end

function read_surface_from_hdf5_file(fname::String)
    coords = read_PestGrid_from_hdf5_file(fname)
    params = read_RuntimeParams_from_hdf5_file(fname)
    norms = read_NormalizingParams_from_hdf5_file(fname)
    imap = read_InverseMap_from_hdf5_file(fname)
    metric = read_Metric_from_hdf5_file(fname)
    geom = read_MagneticGeometry_from_hdf5_file(fname)

    surface = NE3DLESurface(coords,params,norms,imap,metric,geom)
    return surface
end

function read_input_and_surface_from_hdf5_file(fname::String)
    surface = read_surface_from_hdf5_file(fname)
    par_dict = read_input_from_hdf5_file(fname)
    return par_dict,surface
end
