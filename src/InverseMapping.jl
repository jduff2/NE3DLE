#=
    File for performing the inverse mapping for the local flux surface
    cylindrical coordinate representation (R,Z,Φ) in terms of the
    straight-field-line coordinate system (ψ,θ,ζ)

    (R,Z,Φ) = (R(ψ,θ,ζ), Z(ψ,θ,ζ), Φ(ψ,θ,ζ))

    Typically, choose Φ = -ζ
=#


function create_inverse_map(coords::PestGrid{T},R_map::Function,Z_map::Function,Phi_map::Function) where {T}
    inverse_map = InverseMap(coords.M_theta,coords.N_zeta; float_type = T)
   
    inverse_map.R .= R_map.(coords.theta,transpose(coords.zeta))
    inverse_map.Z .= Z_map.(coords.theta,transpose(coords.zeta))
    inverse_map.Phi .= Phi_map.(coords.theta,transpose(coords.zeta))

    compute_derivatives!(coords, inverse_map)

    return inverse_map
end

function create_inverse_map(coords::PestGrid, R_map::Array{T,2}, Z_map::Array{T,2}, Phi_map::Array{T,2}) where {T}

    inverse_map = InverseMap(coords.M_theta,coords.N_zeta, float_type = T)

    inverse_map.R .= R_map
    inverse_map.Z .= Z_map
    inverse_map.Phi .= Phi_map

    compute_derivatives!(coords, inverse_map)

    return inverse_map
end

function create_inverse_map(coords::PestGrid, R_map::Array{T,2}, Z_map::Array{T,2}) where {T}

    inverse_map = InverseMap(coords.M_theta,coords.N_zeta, float_type = T)

    inverse_map.R .= R_map
    inverse_map.Z .= Z_map
    inverse_map.Phi .= repeat(-transpose(coords.zeta),outer=coords.M_theta)

    compute_derivatives!(coords, inverse_map)

    return inverse_map
end

function compute_derivatives!(coords::PestGrid{T}, inverse_map::InverseMap{T}) where {T}
    fft_plan = get_rfft_plan(coords)
    M = coords.M_theta
    N = coords.N_zeta

    fft_d1_theta_coeffs = iseven(M) ? vcat(im * rfftfreq(M-1, M-1),[0.0+im*0.0]) : im * rfftfreq(M, M)
    fft_d1_zeta_coeffs = iseven(N) ? coords.N_field_periods * transpose(im * fftfreq(N, N).*(abs.(fftfreq(N, N)).<div(N,2))) : im * coords.N_field_periods * transpose(fftfreq(N,N))
    fft_d2_theta_coeffs = (im * rfftfreq(M, M)).^2
    fft_d2_zeta_coeffs = (im * coords.N_field_periods * transpose(fftfreq(N, N))).^2
    fft_d2_theta_zeta_coeffs = -rfftfreq(M, M) .* coords.N_field_periods * transpose(fftfreq(N, N))

    R_fft = fft_plan * inverse_map.R
    Z_fft = fft_plan * inverse_map.Z

    inverse_map.R_theta .= fft_plan \ (fft_d1_theta_coeffs .* R_fft)
    inverse_map.R_zeta .= fft_plan \ (fft_d1_zeta_coeffs .* R_fft)
    inverse_map.R_theta_zeta .= fft_plan \ (fft_d2_theta_zeta_coeffs .* R_fft)
    inverse_map.R_theta_theta .= fft_plan \ (fft_d2_theta_coeffs .* R_fft)
    inverse_map.R_zeta_zeta .= fft_plan \ (fft_d2_zeta_coeffs .* R_fft)
    inverse_map.Z_theta .= fft_plan \ (fft_d1_theta_coeffs .* Z_fft)
    inverse_map.Z_zeta .= fft_plan \ (fft_d1_zeta_coeffs .* Z_fft)
    inverse_map.Z_theta_zeta .= fft_plan \ (fft_d2_theta_zeta_coeffs .* Z_fft)
    inverse_map.Z_theta_theta .= fft_plan \ (fft_d2_theta_coeffs .* Z_fft)
    inverse_map.Z_zeta_zeta .= fft_plan \ (fft_d2_zeta_coeffs .* Z_fft)

    nothing
end

function compute_radial_derivatives!(coords::PestGrid{T}, inverse_map::InverseMap{T}) where {T}
    fft_plan = get_rfft_plan(coords)
    M = coords.M_theta
    N = coords.N_zeta

    fft_d1_theta_coeffs = iseven(M) ? vcat(im * rfftfreq(M-1, M-1),[0.0+im*0.0]) : im * rfftfreq(M, M)
    fft_d1_zeta_coeffs = iseven(N) ? coords.N_field_periods * transpose(im * fftfreq(N, N).*(abs.(fftfreq(N, N)).<div(N,2))) : im * coords.N_field_periods * transpose(fftfreq(N,N))

    R_psi_fft = fft_plan * inverse_map.R_psi
    Z_psi_fft = fft_plan * inverse_map.Z_psi

    inverse_map.R_psi_theta .= fft_plan \ (fft_d1_theta_coeffs .* R_psi_fft)
    inverse_map.R_psi_zeta .= fft_plan \ (fft_d1_zeta_coeffs .* R_psi_fft)
    inverse_map.Z_psi_theta .= fft_plan \ (fft_d1_theta_coeffs .* Z_psi_fft)
    inverse_map.Z_psi_zeta .= fft_plan \ (fft_d1_zeta_coeffs .* Z_psi_fft)

    nothing
end

function update_inverse_mapping_fourier_modes!(coords::PestGrid{T},
                                               imap::InverseMap{T},
                                               Rmn::Dict{Tuple{Int,Int},T},
                                               Zmn::Dict{Tuple{Int,Int},T};
                                              ) where {T}
    fft_plan = get_rfft_plan(coords)
    N_zeta = coords.N_zeta
    R_fft = fft_plan*imap.R
    for key in keys(Rmn)
        m = key[1] + 1#mod(div(M_theta - key[1] - 1, M_theta) * M_theta + key[1], M_theta) + 1
        n = mod(div(N_zeta - key[2] - 1, N_zeta) * N_zeta + key[2], N_zeta) + 1
        R_fft[m,n] += Rmn[key]
    end
    imap.R .= real.(fft_plan \ R_fft)
    Z_fft = fft_plan*imap.Z
    for key in keys(Zmn)
        m = key[1] + 1#mod(div(M_theta - key[1] - 1, M_theta) * M_theta + key[1], M_theta) + 1
        n = mod(div(N_zeta - key[2] - 1, N_zeta) * N_zeta + key[2], N_zeta) + 1
        Z_fft[m,n] += im*Zmn[key]
    end
    imap.Z .= real.(fft_plan \ Z_fft)
    compute_derivatives!(coords, imap)
    nothing
end