#=
# Contains the routine for calcluing the Jacobian in the PEST coordinates
# on a magnetic surface
=#

"""
    compute_convolution_arrays(params::RuntimeParams,
                               coords::PestGrid{T},
                               inv_map::InverseMap{T},
                               metric::Metric{T}
                              ) where {T}
Computes the convolution matrix required to solve for the scalar Jacobian in PEST coordinates
on a flux surface.  The inverse Jacobian satisfies the following matrix equation in
wavenumber space: ∑ⱼ∑ₖ(ı m F(m-j,n-k) - ı n G(m-j,n-k))J(j,k) = 0 ∀ m ∈ [-M_θ+1,M_θ-1],
n ∈ [-N_ζ+1,N_ζ-1].  Here F(m,n) = ℱ(g_ζζ + ι g_θζ) and G(m,n) = ℱ(g_θζ + ι g_θθ) where
ℱ(A) is the Fourier transform in θ and ζ of the quantity A.
This function returns the matrix defined by its **unique** entries as determined
by the reality condition F*ᵢⱼ = F₋ᵢ₋ⱼ for real quantities.  For an inverse mapping defined
on a (M_θ x N_ζ) grid, the convolution matrix has the following indexing:\n
|       Indices      | (j=0,k=0) | (j=1,k=0) | ... | (j=M_θ-1,k=0) | (j=-M_θ+1,k=1) | ... | (j=M_θ-1,k=1) | ... | (j=M_θ-1,k=N_ζ-1) |
|:-----------------:|:---------:|:---------:|:---:|:-------------:|:--------------:|:---:|:-------------:|:---:|:-----------------:|
|     (m=0,n=0)     |     x     |     x     | ... |        x      |        x       | ... |        x      | ... |         x         |
|     (m=1,n=0)     |     x     |     x     | ... |        x      |        x       | ... |        x      | ... |         x         |
|         .         |     x     |     x     | ... |        x      |        x       | ... |        x      | ... |         x         |
|         .         |     x     |     x     | ... |        x      |        x       | ... |        x      | ... |         x         |
|         .         |     x     |     x     | ... |        x      |        x       | ... |        x      | ... |         x         |
|   (m=M_θ-1,n=0)   |     x     |     x     | ... |        x      |        x       | ... |        x      | ... |         x         |
|  (m=-M_θ+1,n=1)   |     x     |     x     | ... |        x      |        x       | ... |        x      | ... |         x         |
|         .         |     x     |     x     | ... |        x      |        x       | ... |        x      | ... |         x         |
|         .         |     x     |     x     | ... |        x      |        x       | ... |        x      | ... |         x         |
|         .         |     x     |     x     | ... |        x      |        x       | ... |        x      | ... |         x         |
|   (m=M_θ-1,n=1)   |     x     |     x     | ... |        x      |        x       | ... |        x      | ... |         x         |
|         .         |     x     |     x     | ... |        x      |        x       | ... |        x      | ... |         x         |
|         .         |     x     |     x     | ... |        x      |        x       | ... |        x      | ... |         x         |
|         .         |     x     |     x     | ... |        x      |        x       | ... |        x      | ... |         x         |
| (m=M_θ-1,n=N_ζ-1) |     x     |     x     | ... |        x      |        x       | ... |        x      | ... |         x         |\n
Here, the poloidal indices are treated as the unique indices and the toroidal indices are what determines
the reality condition.  The ordering of the unique components of the inverse Jacobian are the
same as the row/column ordering of the convolution matrix
# Returns
- H_mat : the convolution matrix in reduced form\n
- b : the right hand side vector\n
- fft_inds : poloidal and toroidal indices for each entry of the inverse Jacobian vector in order

# See also: [`convolution_kernel`](@ref), [`convolution_vector`](@ref)
"""
function compute_convolution_arrays(params::RuntimeParams,
                                    coords::PestGrid{T},
                                    metric::Metric{T}
                                   ) where {T}
    F_fft, G_fft = build_operators(params, coords, metric)
    H = convolution_kernel(F_fft, G_fft, nfp = coords.N_field_periods)
    b, indices = convolution_vector(F_fft, G_fft, nfp = coords.N_field_periods)
    return H, b, indices
end

"""
    check_jacobian(params::RuntimeParams,
                   coords::PestGrid{T},
                   metric::Metric{T}
                  ) where {T}
Check the that the local 3D equilibrium Jacobian computed by compute_convolution_matrix!() and compute_jacobian!() is a valid solution.
# Returns
- Array with the value of the LHS - RHS of Hegna, Phys. Plasmas, 7(10), 3922-3928, 2000; Equation 14 at every point on the (θ,ζ) grid
"""
function check_jacobian(params::RuntimeParams,
                        coords::PestGrid{T},
                        metric::Metric{T}
                       ) where {T}
    fft_plan = get_rfft_plan(coords)
    iota = params.iota
    #Define local variables with the grid sizes for convenience
    M = coords.M_theta
    N = coords.N_zeta
    #Check the bounds to make sure all the objects are of the same size
    if (M,N) != size(metric.sqrtg); error("PestGrid $(:coords) and
        Metric $(:metric) have incompatible dimensions!"); end;
    #Define the F and G objects
    fft_d1_theta_coeffs = iseven(M) ? vcat(im * rfftfreq(M-1, M-1),[0.0+im*0.0]) : im * rfftfreq(M, M)
    fft_d1_zeta_coeffs = iseven(N) ? coords.N_field_periods * transpose(im * fftfreq(N, N).*(abs.(fftfreq(N, N)).<div(N,2))) : im * coords.N_field_periods * transpose(fftfreq(N,N))

    F = @. (metric.g_zeta_zeta + iota*metric.g_theta_zeta)/metric.sqrtg
    G = @. (metric.g_theta_zeta + iota*metric.g_theta_theta)/metric.sqrtg

    F_fft = fft_plan*F
    G_fft = fft_plan*G
    F_theta = fft_plan \ (fft_d1_theta_coeffs .* F_fft)
    G_zeta = fft_plan \ (fft_d1_zeta_coeffs .* G_fft)

    return F_theta .- G_zeta
end


function build_operators(params::RuntimeParams,
                         coords::PestGrid{T},
                         metric::Metric{T},
                        ) where {T}
    fft_plan = get_fft_plan(coords)

    F = @. metric.g_zeta_zeta + params.iota * metric.g_theta_zeta
    G = @. metric.g_theta_zeta + params.iota * metric.g_theta_theta

    F_fft = fft_plan * F
    G_fft = fft_plan * G

    return real(F_fft), real(G_fft)
end

function convolution_kernel(F::AbstractArray{T},
                            G::AbstractArray{T};
                            nfp::Integer = 1,
                           ) where {T}
    size(F) == size(G) || throw(DimensionMismatch("Input arrays are incompatible!"))
    (M, N) = size(F)
    M_min = -div(M, 2) + iseven(M)
    M_max = M + M_min - 1
    N_min = -div(N, 2) + iseven(N)
    N_max = N + N_min - 1
    dim_size = N_max * M + M_max
    H = Array{T}(undef, dim_size, dim_size)
    divmod_M = M_max - iseven(M)
    for i in eachindex(H)
        row = (i - 1) % dim_size + 1
        col = div(i - 1, dim_size) + 1
        q = div(row + divmod_M, M)
        p = mod(row + divmod_M, M) + M_min
        n = div(col + divmod_M, M)
        m = mod(col + divmod_M, M) + M_min
        ppm = p + m
        pmm = p - m
        qpn = q + n
        qmn = q - n
        p_plus_m = mod(div(M - ppm - 1, M) * M + ppm, M) + 1
        p_minus_m = mod(div(M - pmm - 1, M) * M + pmm, M) + 1
        q_plus_n = mod(div(N - qpn - 1, N) * N + qpn, N) + 1
        q_minus_n = mod(div(N - qmn - 1, N) * N + qmn, N) + 1
        plus_flag = (ppm <= M_max && ppm >= M_min) * (qpn <= N_max && qpn >= N_min)
        minus_flag = (pmm <= M_max && pmm >= M_min) * (qmn <= N_max && qmn >= N_min)
        F_plus = convert(T, plus_flag * (p + 2*m))
        F_minus = convert(T, minus_flag * (p - 2*m))
        G_plus = convert(T, plus_flag * nfp * (q + 2*n))
        G_minus = convert(T, minus_flag * nfp * (q - 2*n))
        @inbounds H[i] = (F_minus * F[p_minus_m, q_minus_n] + F_plus * F[p_plus_m, q_plus_n]
                          - G_minus * G[p_minus_m,q_minus_n] - G_plus * G[p_plus_m,q_plus_n])
    end
    return H
end

function convolution_vector(F::AbstractArray{T},
                            G::AbstractArray{T};
                            nfp::Integer = 1,
                           ) where {T}
    size(F) == size(G) || throw(DimensionMismatch("Input arrays are incompatible!"))
    (M, N) = size(F)
    M_min = -div(M, 2) + iseven(M)
    M_max = M + M_min - 1
    N_min = -div(N, 2) + iseven(N)
    N_max = N + N_min - 1
    dim_size = N_max * M + M_max
    b = Vector{T}(undef, dim_size)
    indices = Matrix{Int}(undef, (2,dim_size))
    divmod_M = M_max - iseven(M)
    for i in eachindex(b)
        row = (i - 1) % dim_size + 1
        q = div(row + divmod_M, M)
        p = mod(row + divmod_M, M) + M_min
        p_minus_m = mod(div(M - p - 1, M) * M + p, M) + 1
        q_minus_n = mod(div(N - q - 1, N) * N + q, N) + 1
        plus_flag = (p <= M_max && p >= M_min) * (q <= N_max && q >= N_min)
        minus_flag = (p <= M_max && p >= M_min) * (q <= N_max && q >= N_min)
        F_minus = convert(T, minus_flag * (p))
        G_minus = convert(T, minus_flag * nfp * (q))
        @inbounds b[i] = -F_minus * F[p_minus_m, q_minus_n] + G_minus * G[p_minus_m, q_minus_n]
        @inbounds indices[:,i] .= (p,q)
    end
    return b, indices
end


function compute_jacobian!(H::Matrix{T},
                           b::Vector{T},
                           indices::Matrix{Int},
                           coords::PestGrid{T},
                           metric::Metric{T}
                          ) where {T}
    jac_fft = H \ b
    M = coords.M_theta
    N = coords.N_zeta
    M_min = -div(M, 2) + iseven(M)
    M_max = M + M_min - 1
    N_min = -div(N, 2) + iseven(N)
    N_max = N + N_min - 1
    jac = ones(T, (M, N))
    θ = coords.theta
    ζ = transpose(coords.zeta)
    Nt = coords.N_field_periods
    @inbounds for i in eachindex(jac_fft)
        m = indices[1, i]
        n = indices[2, i]
        jac .+= 2 * jac_fft[i] * cos.(m * θ .+ Nt * n * ζ)
    end
    metric.sqrtg .= jac

    return nothing
end
