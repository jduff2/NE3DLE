using NE3DLE
using Test
using VMEC
using StellaratorOptimization
using PlasmaEquilibriumToolkit

@testset "NE3DLE" begin
    @testset "Cyclone Base Case" begin
        surface = call_NE3DLE("CBC.in");
        field_line = NE3DLEFieldLine(surface);
        @testset "Axisymmetric jacobian" begin
            @test all(surface.imap.R.*(surface.imap.R_psi.*surface.imap.Z_theta.-surface.imap.R_theta.*surface.imap.Z_psi) .≈ surface.metric.sqrtg)
            @test all(surface.imap.R.^2 ./surface.metric.sqrtg .≈ (surface.imap.R.^2 ./surface.metric.sqrtg)[1,1])
        end
        @testset "Input Options" begin
            sigma = surface.params.surface_quant["surface_current"]
            grad_p = surface.params.surface_quant["grad_p"]
            grad_iota = surface.params.surface_quant["grad_iota"]
            cfp = surface.params.computed_field_periods
            @testset "n_pol/computed_field_periods" begin
                NE3DLE.swap_input_variables("CBC.in","n_pol","computed_field_periods = $cfp");
                surface1 = call_NE3DLE("CBC.in");
                field_line1 = NE3DLEFieldLine(surface1);
                NE3DLE.swap_input_variables("CBC.in","computed_field_periods","n_pol = 1");
                @test all(surface1.metric.sqrtg .≈ surface.metric.sqrtg);
                @test all(field_line1.gene.jac .≈ field_line.gene.jac);
            end
            @testset "sigma and grad_p" begin
                NE3DLE.swap_input_variables("CBC.in","s_param","sigma = $sigma")
                NE3DLE.swap_input_variables("CBC.in","alpha_param","grad_p = $grad_p")
                surface1 = call_NE3DLE("CBC.in");
                field_line1 = NE3DLEFieldLine(surface1);
                NE3DLE.swap_input_variables("CBC.in","sigma","s_param = 0.78")
                NE3DLE.swap_input_variables("CBC.in","grad_p","alpha_param = 0.1")
                @test all(surface1.geom.local_shear .≈ surface.geom.local_shear);
                @test all(field_line1.fl.local_shear .≈ field_line.fl.local_shear);
            end

            @testset "sigma and grad_iota" begin
                NE3DLE.swap_input_variables("CBC.in","alpha_param","sigma = $sigma")
                NE3DLE.swap_input_variables("CBC.in","s_param","grad_iota = $grad_iota")
                surface1 = call_NE3DLE("CBC.in");
                field_line1 = NE3DLEFieldLine(surface1);
                NE3DLE.swap_input_variables("CBC.in","sigma","alpha_param = 0.1")
                NE3DLE.swap_input_variables("CBC.in","grad_iota","s_param = 0.78")
                @test all(surface1.geom.local_shear .≈ surface.geom.local_shear);
                @test all(field_line1.fl.local_shear .≈ field_line.fl.local_shear);
            end
            @testset "grad_p and grad_iota" begin
                NE3DLE.swap_input_variables("CBC.in","alpha_param","grad_p = $grad_p")
                NE3DLE.swap_input_variables("CBC.in","s_param","grad_iota = $grad_iota")
                surface1 = call_NE3DLE("CBC.in");
                field_line1 = NE3DLEFieldLine(surface1);                
                NE3DLE.swap_input_variables("CBC.in","grad_p","alpha_param = 0.1")
                NE3DLE.swap_input_variables("CBC.in","grad_iota","s_param = 0.78")
                @test all(surface1.geom.local_shear .≈ surface.geom.local_shear);
                @test all(field_line1.fl.local_shear .≈ field_line.fl.local_shear);
            end
            @testset "grad_p and s_param" begin
                NE3DLE.swap_input_variables("CBC.in","alpha_param","grad_p = $grad_p")
                surface1 = call_NE3DLE("CBC.in");
                field_line1 = NE3DLEFieldLine(surface1);                
                NE3DLE.swap_input_variables("CBC.in","grad_p","alpha_param = 0.1")
                @test all(surface1.geom.local_shear .≈ surface.geom.local_shear);
                @test all(field_line1.fl.local_shear .≈ field_line.fl.local_shear);
            end
            @testset "grad_iota and alpha_param" begin
                NE3DLE.swap_input_variables("CBC.in","s_param","grad_iota = $grad_iota")
                surface1 = call_NE3DLE("CBC.in");
                field_line1 = NE3DLEFieldLine(surface1);                
                NE3DLE.swap_input_variables("CBC.in","grad_iota","s_param = 0.78")
                @test all(surface1.geom.local_shear .≈ surface.geom.local_shear);
                @test all(field_line1.fl.local_shear .≈ field_line.fl.local_shear);
            end
            @testset "sigma and s_param" begin
                NE3DLE.swap_input_variables("CBC.in","alpha_param","sigma = $sigma")
                surface1 = call_NE3DLE("CBC.in");
                field_line1 = NE3DLEFieldLine(surface1);                
                NE3DLE.swap_input_variables("CBC.in","sigma","alpha_param = 0.1")
                @test all(surface1.geom.local_shear .≈ surface.geom.local_shear);
                @test all(field_line1.fl.local_shear .≈ field_line.fl.local_shear);
            end
            @testset "sigma and alpha_param" begin
                NE3DLE.swap_input_variables("CBC.in","s_param","sigma = $sigma")
                surface1 = call_NE3DLE("CBC.in");
                field_line1 = NE3DLEFieldLine(surface1);                
                NE3DLE.swap_input_variables("CBC.in","sigma","s_param = 0.78")
                @test all(surface1.geom.local_shear .≈ surface.geom.local_shear);
                @test all(field_line1.fl.local_shear .≈ field_line.fl.local_shear);
            end
            @testset "s_param and alpha_param" begin
                surface1 = call_NE3DLE("CBC.in");
                field_line1 = NE3DLEFieldLine(surface1);                
                @test all(surface1.geom.local_shear .≈ surface.geom.local_shear);
                @test all(field_line1.fl.local_shear .≈ field_line.fl.local_shear);
            end
        end
        @testset "even/odd modes" begin
            NE3DLE.swap_input_variables("CBC.in","M_theta","M_theta = 63")
            surface1 = call_NE3DLE("CBC.in");
            field_line1 = NE3DLEFieldLine(surface1);
            NE3DLE.swap_input_variables("CBC.in","M_theta","M_theta = 64")
            @test 1e-14 > (maximum(abs.(field_line1.gene.jac .- field_line.gene.jac)))
        end
        @testset "update par_dict" begin
            par_dict = NE3DLE.read_par_file("CBC.in")
            surface1 = call_NE3DLE(par_dict);
            field_line1 = NE3DLEFieldLine(surface1);
            @test all(surface1.metric.sqrtg .≈ surface.metric.sqrtg);
            @test all(field_line1.gene.jac .≈ field_line.gene.jac);
            @test all(surface1.geom.local_shear .≈ surface.geom.local_shear);
            @test all(field_line1.fl.local_shear .≈ field_line.fl.local_shear);
            par_dict["s_param"] *= 2
            surface2 = call_NE3DLE(par_dict);
            field_line2 = NE3DLEFieldLine(surface2);
            @test all(surface2.metric.sqrtg .≈ surface.metric.sqrtg);
            @test all(field_line2.gene.jac .≈ field_line.gene.jac);
            @test all(surface2.geom.local_shear .!= surface.geom.local_shear);
            @test all(field_line2.fl.local_shear .!= field_line.fl.local_shear);
        end
    end
    @testset "QHS_good" begin
        @testset "wout file from input file" begin
            vmec = VMEC.readVmecWout("wout_QHS_good.nc");
            surface = call_NE3DLE("qhs_good.in");
            field_line = NE3DLEFieldLine(surface);
            s = (surface.norms.ρ/surface.norms.L_ref)^2;
            vmec_s = VMEC.VmecSurface(s,vmec);
            M_theta = surface.params.M_theta
            N_zeta = surface.params.N_zeta
            @test 1e-4>maximum(abs.(NE3DLE.check_jacobian(surface.params,surface.coords,surface.metric)))
            i = rand(1:M_theta)
            j = rand(1:N_zeta)
            thetaRange = 0:2π/M_theta:2π-2π/M_theta; zetaRange = (0:2π/N_zeta:2π-2π/N_zeta)./vmec_s.nfp;
            flux_coords = MagneticCoordinateGrid(FluxCoordinates,vmec_s.s*vmec_s.phi[end]/(2π)*vmec_s.signgs,thetaRange,zetaRange);
            vmec_basis = VMEC.transform_deriv(CylindricalFromFlux(),flux_coords[j,i],vmec_s)
            @test field_line.gene.shat ≈ VMEC.shat(vmec_s)
            @test vmec_basis[1,2] - surface.imap.R_theta[i,j] < 1e-5
            @test vmec_basis[1,3] - surface.imap.R_zeta[i,j] < 1e-5
            @test vmec_basis[3,2] - surface.imap.Z_theta[i,j] < 1e-5
            @test vmec_basis[3,3] - surface.imap.Z_zeta[i,j] < 1e-5
            @test all(surface.imap.R.*(surface.imap.R_psi.*surface.imap.Z_theta.-surface.imap.R_theta.*surface.imap.Z_psi) .≈ surface.metric.sqrtg)
            par_dict = NE3DLE.read_par_file("qhs_good.in")
            NE3DLE.initialize_from_eq!(par_dict,vmec_s)
            surface1 = call_NE3DLE(par_dict);
            field_line1 = NE3DLEFieldLine(surface1);
            @test all(surface1.metric.sqrtg .≈ surface.metric.sqrtg);
            @test all(field_line1.gene.jac .≈ field_line.gene.jac);
            @test maximum(abs.(surface1.geom.local_shear .- surface.geom.local_shear)) < 1e-10
            @test all(field_line1.fl.local_shear .≈ field_line.fl.local_shear);
            par_dict["s_param"] *= 2
            surface2 = call_NE3DLE(par_dict);
            field_line2 = NE3DLEFieldLine(surface2);
            @test all(surface2.metric.sqrtg .≈ surface.metric.sqrtg);
            @test all(field_line2.gene.jac .≈ field_line.gene.jac);
            @test all(surface2.geom.local_shear .!= surface.geom.local_shear);
            @test all(field_line2.fl.local_shear .!= field_line.fl.local_shear);
        end
    end
    @testset "Miller" begin
        NE3DLE_exec();
        @test isdir("./delta_scan")
        @test isdir("./delta_scan/delta_1")
        @test isdir("./delta_scan/delta_2")
        @test isdir("./delta_scan/delta_3")
        par_dict1 = NE3DLE.read_par_file("./delta_scan/delta_1/NE3DLE.in")
        par_dict2 = NE3DLE.read_par_file("./delta_scan/delta_2/NE3DLE.in")
        par_dict3 = NE3DLE.read_par_file("./delta_scan/delta_3/NE3DLE.in")
        @test par_dict1["delta"][1] == -0.85
        @test par_dict2["delta"][1] == 0
        @test par_dict3["delta"][1] == 0.85
        delete!(par_dict1,"delta")
        delete!(par_dict2,"delta")
        delete!(par_dict3,"delta")
        @test par_dict1==par_dict2&&par_dict1==par_dict3
        @testset "update par_dict" begin
            surface = call_NE3DLE("./delta_scan/delta_1/NE3DLE.in");
            field_line = NE3DLEFieldLine(surface);
            par_dict = NE3DLE.read_par_file("./delta_scan/delta_1/NE3DLE.in")
            surface1 = call_NE3DLE(par_dict);
            field_line1 = NE3DLEFieldLine(surface1);
            @test all(surface1.metric.sqrtg .≈ surface.metric.sqrtg);
            @test all(field_line1.gene.jac .≈ field_line.gene.jac);
            @test all(surface1.geom.local_shear .≈ surface.geom.local_shear);
            @test all(field_line1.fl.local_shear .≈ field_line.fl.local_shear);
            par_dict["s_param"] *= 2
            surface2 = call_NE3DLE(par_dict);
            field_line2 = NE3DLEFieldLine(surface2);
            @test all(surface2.metric.sqrtg .≈ surface.metric.sqrtg);
            @test all(field_line2.gene.jac .≈ field_line.gene.jac);
            @test all(surface2.geom.local_shear .!= surface.geom.local_shear);
            @test all(field_line2.fl.local_shear .!= field_line.fl.local_shear);
        end
    end
    if isdir("./delta_scan")
        rm("./delta_scan", recursive=true)
    end
    @testset "utilities" begin
        surface = call_NE3DLE("qhs_good.in");
        @test all(NE3DLE.inverse_Boozer_spectrum(NE3DLE.Boozer_spectrum(surface),surface) .- surface.geom.B_mag .<= 5e-4)
    end
end
